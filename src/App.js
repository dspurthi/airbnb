import React from "react";
import "./App.css";
import { Navbar } from "./components/navbar";
import { Hero } from "./components/hero";
import { Card } from "./components/card";
import data from "./components/data";

console.log(data);

function App() {
  const cards = data.map((item) => {
    return (
      <Card
        key={item.id}
        {...item}
      />
    );
  });
  return (
    <div>
      <Navbar />
      <Hero />
      <div className="card-whole">{cards}</div>
    </div>
  );
}

export default App;
