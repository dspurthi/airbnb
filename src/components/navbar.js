import React from "react";
import logo from "../airbnb.png";

import "./navbar.css"

function Navbar() {
  return (
    <nav className="top-bar">
      <img src={logo}  alt="logo" />
    </nav>
  );
}

export { Navbar };
