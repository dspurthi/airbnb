let data = [
  {
    id: 1,
    img: "./home1.jpeg",
    star:"./star.png",
    text: "Beautiful house",
    rating: 5,
    number: 50,
    country: "AU",
    price: 230,
    headcount: "/Day",
    status:new Date().getDay()
  },
  {
    id: 2,
    img: "./home2.jpg",
    star:"./star.png",
    text: "House with Swimming Pool",
    rating: 5,
    number: 150,
    country: "AU",
    price: 200,
    headcount: "/Day",
    status:new Date().getDay()
  },
  {
    id: 3,
    img: "./home3.jpg",
    star:"./star.png",
    text: "Single house",
    rating: 5,
    number: 200,
    country: "AU",
    price: 220,
    headcount: "/Day",
    status:new Date().getDay()
  },
];

export default data