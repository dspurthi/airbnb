import React from "react";
import "./hero.css";
import photogrid from "../grid.png";

function Hero() {
  return (
    <section className="hero">
      <img src={photogrid} alt="grid" className="hero--photo"/>
      <h1 className="hero--title">Online Experiences</h1>
      <p className="hero--text">
        Join unique interactive activities led by one-of-a-kind hosts—all
        without leaving home.
      </p>
    </section>
  );
}

export {Hero}
