import React from "react";
import "./card.css";

function Card(props) {
  let badgeText 
  if (props.status ===6 || props.status===7){
    badgeText ="Booked"
  }else{
    badgeText = "Available"
  }

  
  return (
    <div className="card">
      <div className="card-badge">{badgeText}</div>
      <img className="thumb" src={props.img} alt="pic" />

      <div className="card-para">
        <div className="card--stats">
          <img src={props.star} width={14} height={14} alt="star" />
          <span>{props.rating}</span>
          <span className="grey">({props.number}) .</span>
          <span className="grey">{props.country}</span>
        </div>
        <p>{props.text}</p>
        <p>
          <span className="bold">from ${props.price}</span>
          {props.headcount}
        </p>
      </div>
    </div>
  );
}

export { Card };
